﻿(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        $('#get-weather-btn').click(getWeatherWithZipCode);
        $('#get-weather-btn').click(function () {
            var color = "";
            var r = Math.floor(Math.random() * 255) + 1;
            var g = Math.floor(Math.random() * 255) + 1;
            var b = Math.floor(Math.random() * 255) + 1;
            color += "rgb(" + r + ", " + g + ", " + b +")";
            $('.header').css("background-color", color);
            $('.footer').css({"background-color": color,
                            "color": "#ffffff"});
        });
        getWeatherWithGeoLocation();
    }

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

} )();